<!DOCTYPE html>
<html>

<head>
	<title>Register</title>
</head>


<body>
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	
	<!--Membuat Form -->
	<form action= "/send" method="POST">
        @csrf
		<label>First Name</label>
		<br><br>
		<input name="firstname"></input>
		<br><br>
		<label name="lastname">Last Name</label>
		<br><br>
		<input name="lastname"></input>
		<br><br>
		<label>Gender</label>
		<br><br>
		<input type="radio" name="gender_user" value="0" checked> Male <br>
		<input type="radio" name="gender_user" value="1"> Female <br>
		<input type="radio" name="gender_user" value="2"> Other <br>
		<br>
		
		<label>Nationality</label>
		<br><br>
		<select>
			<option value="0">Indonesian</option>
			<option value="1">Malaysian</option>
			<option value="2">Singapure</option>
			<option value="3">Other</option>
		</select>
		<br><br>
		<label>Language Spoken</label>
		<br><br>
		<input type="checkbox" name="language_user" value="0" checked> Bahasa Indonesia <br>
		<input type="checkbox" name="language_user" value="1"> English <br>
		<input type="checkbox" name="language_user" value="2"> Other <br>
		<br>
		<label for="Bio_user">Bio : </label>
		<br><br>
		<textarea cols="30" rows="10" id="Bio_user"></textarea>
		<br>
		
		<input type="submit" value="send">
		
	
	</form>

</body>

</html>
