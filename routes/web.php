<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*TUGAS LARAVEL WEB STATIS MVC
Route::get('/', function () {
    return view('utama');
});

Route::get('/home', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/send', 'AuthController@send');
Route::get('/welcome', 'AuthController@welcome');
Route::get('/master', function(){
    return view('adminlte.master');
});
*/

//LATIHAN LARAVEL TUTORIAL

Route::get('/items', function(){
    return view('items.index');
});


Route::get('/items/create', function(){
    return view('items.create');
});

//TUGAS LARAVEL 2

Route::get('/', function(){
    return view('tables');
});

Route::get('/data-tables', function(){
    return view('data-tables');
});

//Laravel CRUD dengan Query Builder
Route::get('/pertanyaan', 'PertanyaanController@index');

Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');